import numpy as np
import pylab

def a(x,t):
    return -1.0*x

def b(x,t):
    return 1.0

def euler_maruyama(x,a,b,dt,T,tdump):
    """
    Solve the stochastic differential equation

    dx = a(x,t)dt + b(x,t)dW

    using the Euler-Maruyama method

    x^{n+1} = x^n = a(x^n,t^n)dt + b(x^n,t^n)dW

    inputs:
    x - initial condition for x
    a - a function that takes in x,t and returns a(x,t)
    b - a function that takes in x,t and returns b(x,t)
    dt - the time stepsize
    T - the time limit for the simulation
    tdump - the time interval between storing values of x

    outputs:
    xvals - a numpy array containing the values of x at the chosen time points
    tvals - a numpy array containing the values of t at the same points
    Wvals - a numpy array containing the values of W at the same points
    """

    xvals = [x]
    t = 0.
    tvals = [0]
    Wvals = [0]
    W = 0.
    dumpt = 0.
    while(t<T-0.5*dt):
        dW = dt**0.5*np.random.randn()
        x += dt*a(x,t) + b(x,t)*dW

        W += dW
        t += dt
        dumpt += dt

        if(dumpt>tdump-0.5*dt):
            dumpt -= tdump
            xvals.append(x)
            tvals.append(t)
            Wvals.append(W)
    return np.array(xvals), np.array(tvals), np.array(Wvals)

x,t,W = euler_maruyama(0.0,a,b,dt=0.001,T=10.0,tdump=0.01)
pylab.plot(t,x)
pylab.show()
